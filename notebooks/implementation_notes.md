---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

$$
\newcommand{bra}[1]{\langle #1 |}
\newcommand{ket}[1]{| #1 \rangle}
\newcommand{braket}[1]{\langle #1 | #1 \rangle}
\newcommand{ketbra}[1]{| #1 \rangle \langle #1|}
\newcommand{op}[1]{\underline{#1}}
\newcommand{I}{\op{\mathrm{I}}}
$$

# Implementing weak measurement


## The weak measurement operators

Our goal is to implement the weak measurement operators ${\op{M_m}}$, which expressed in the computational basis are:

$$
\op{M_0} = \frac{1}{\sqrt{1 + \alpha^2}}
							 										\begin{bmatrix}
                                                                    1 & 0 \\
                                                                    0 & \alpha
                                                                    \end{bmatrix}
\qquad
\op{M_1} = \frac{1}{\sqrt{1 + \alpha^2}}
							 										\begin{bmatrix}
                                                                    \alpha & 0 \\
                                                                    0 & 1
                                                                    \end{bmatrix}
$$

where $\alpha \in [0, 1]$.  $\alpha=0$ corresponds to a strong measurement, while $\alpha=1$ corresponds to a totally uninformative measurement.

If our state is

$$
\ket{\psi} = \psi_0 \ket{0} + \psi_1 \ket{1}
$$

the probabilities of each measurement outcome and states after measurement are:

$$
p(m=0 | \psi) \equiv p_0 = \frac{1}{1 + \alpha^2} \left( |\psi_0|^2 + \alpha^2 |\psi_1|^2 \right)  \qquad  \ket{\psi} \rightarrow \ket{\psi_0'} \equiv \frac{1}{\sqrt{|\psi_0|^2 + \alpha^2 |\psi_1|^2}} \left( \psi_0 \ket{0} + \alpha \psi_1 \ket{1} \right) \\
p(m=1 | \psi) \equiv p_1 = \frac{1}{1 + \alpha^2} \left( \alpha^2 |\psi_0|^2 + |\psi_1|^2 \right)  \qquad  \ket{\psi} \rightarrow \ket{\psi_1'} \equiv \frac{1}{\sqrt{\alpha^2 |\psi_0|^2 + |\psi_1|^2}} \left( \alpha \psi_0 \ket{0} + \psi_1 \ket{1} \right)
$$

+++

## Implementation with projective measurements


We can implement the weak measurement with a circuit that uses an ancilla qubit, making a strong measurement on the ancilla instead of the primary state.  If our circuit produces the state (before measurement)

$$
| \psi_f \rangle = \sqrt{p_0} \ket{\psi_0'} \otimes \ket{0} + \sqrt{p_1} \ket{\psi_1'} \otimes \ket{1}
$$

then a measurement of the ancilla qubit will leave our primary state in the proper state for our weak measurement operator, and with the proper probabilities.

The operators ${\op{M_n'}}$ for the strong measurment on the ancilla qubit are

$$
\op{M_0'} = \I \otimes \ketbra{0} \qquad \op{M_1'} = \I \otimes \ketbra{1}
$$

Thus the probabilities of each measurement outcome and states after measurement are:

$$
p(n=0 | \psi_f) = p_0 \qquad \ket{\psi_f} \rightarrow \ket{\psi_0'} \otimes \ket{0} \\
p(n=1 | \psi_f) = p_1 \qquad \ket{\psi_f} \rightarrow \ket{\psi_1'} \otimes \ket{1} \\
$$

<!-- For example, if we measure the ancilla to be $\ket{0}$:
$$
\begin{aligned}
p(n=0 | \psi_f) &= \bra{\psi_f} \left( \I \otimes \ketbra{0} \right)^2 \ket{\psi_f}\\
                &= \bra{\psi_f} \left( \sqrt{p_0} \ket{\psi_0'} \otimes \ket{0} \right) \\
                &= p_0 \\
\end{aligned}
$$
$$
\begin{aligned}
\ket{\psi} \rightarrow &\sqrt{\frac{1}{p_0}} \left( \I \otimes \ketbra{0} \right) \left(\sqrt{p_0} \ket{\psi_0'} \otimes \ket{0} + \sqrt{p_1} \ket{\psi_1'} \otimes \ket{1} \right) \\
                       &= \ket{\psi_0'} \otimes \ket{0}
\end{aligned}
$$ -->

+++

### Constructing the circuit

Explicitly, the state we need our circuit to create before measuring the ancilla qubit is:

$$
\ket{\psi_f} = \sqrt{\frac{1}{1 + \alpha^2}} \left( \psi_0 \ket{00} + \alpha \psi_0 \ket{01} + \alpha \psi_1 \ket{10} + \psi_1 \ket{00} \right)
$$

With some work, we find this can be achieved through the following circuit:

<img src="../tex/z_circuit/circuit.png" width="400" />

The rotation operator about the $y$ axis is:

$$
\op{R_y}(\theta) = \begin{bmatrix}
                   \cos{\theta/2} & -\sin{\theta/2} \\
                   \sin{\theta/2} & \cos{\theta/2}
                   \end{bmatrix}
$$

Plugging in $\theta = 2 \tan^{-1}{\alpha}$ gives:

$$
\op{R_y}(2 \tan^{-1}{\alpha}) = \sqrt{\frac{1}{1 + \alpha^2}} \begin{bmatrix}
                   1 & -\alpha \\
                   \alpha & 1
                   \end{bmatrix}
$$

The action of this operator on the initial ancilla state is therefore:

$$
\op{R_y}(2 \tan^{-1}{\alpha}) \ket{0} = \sqrt{\frac{1}{1 + \alpha^2}} \begin{bmatrix}
                   1 & -\alpha \\
                   \alpha & 1
                   \end{bmatrix}
                   \begin{bmatrix} 1 \\ 0 \end{bmatrix} \\
                 = \sqrt{\frac{1}{1 + \alpha^2}} \begin{bmatrix} 1 \\ \alpha \end{bmatrix}
$$

Thus the intermediate state $\ket{\psi^{(0)}}$ is:

$$
\ket{\psi^{(0)}} = \ket{\psi} \otimes \left( \op{R_y}(2 \tan^{-1}{\alpha}) \ket{0} \right)
                 = \begin{bmatrix} \psi_0 \\ \psi_1 \end{bmatrix} \otimes \sqrt{\frac{1}{1 + \alpha^2}} \begin{bmatrix} 1 \\ \alpha \end{bmatrix}
                 = \sqrt{\frac{1}{1 + \alpha^2}} \begin{bmatrix} \psi_0 \\ \alpha \psi_0 \\ \psi_1 \\ \alpha \psi_1 \end{bmatrix}
$$

And so the intermediate state $\ket{\psi^{(1)}}$ is:

$$
\ket{\psi^{(1)}} = \sqrt{\frac{1}{1 + \alpha^2}} \begin{bmatrix} \psi_0 \\ \alpha \psi_0 \\ \alpha \psi_1 \\ \psi_1 \end{bmatrix}
$$

We see $\ket{\psi^{(1)}} = \ket{\psi_f}$, so strong measurement on the ancilla qubit after applying this circuit will enact weak measurement on the primary state, as demonstrated above.

+++

### Measurement in a different basis

If we want to perform a weak measurement in a basis other than $\mathrm{Z}$, we can simply rotate our primary state into the $\mathrm{Z}$ basis, make the weak measurement as before, and then undo the rotation on the primary state.

For example, to measure in a different basis in the X-Z plane:

<img src="../tex/general_circuit/circuit.png" width="400" />

Here, $\vartheta$ represents the polar angle of the axis in the X-Z plane in which basis we measure: so $\vartheta=0$ represents measuring in the $\mathrm{Z}$ basis, $\vartheta=\pi/2$ represents measuring in the $\mathrm{X}$ basis, etc.

+++

## Implementation in Qiskit

```{code-cell} ipython3
import qiskit
import qiskit.tools.jupyter
%qiskit_version_table
```

```{code-cell} ipython3
import numpy as np
from qiskit import QuantumCircuit
from qiskit.extensions import Initialize

def get_weak_measurement_qc(psi=None, alpha=0.5, theta=0):
    """Return a quantum circuit performing a weak measurement in the X-Z plane.
    
    Parameters
    ----------
    psi : qiskit.quantum_info.Statevector or length-2 list of complex floats, optional
        The state to measure.  Defaults to [1, 0] (the ket |0>).
    alpha : float in range [0, 1]
        Controls the "weakness" of the measurement.  `alpha=0` corresponds
        to a strong measurment, `alpha=1` to a completely uninformative
        measurement.
    theta : float between 0 and pi, optional
        The polar angle of the axis (in the X-Z plane) in which basis we measure.
    
    Returns
    -------
    qiskit.QuantumCircuit
        The quantum circuit for this weak measurement.
    """
    gamma = 2 * np.arctan(alpha)
    
    if psi is None:
        psi = [1, 0]
        
    else:
        # ensure psi is normalized
        psi = np.asarray(psi) / np.linalg.norm(psi)
        
    init_gate = Initialize(psi)
    init_gate.label = r'$\psi$'

    qc = QuantumCircuit(2, 1)
    qc.append(init_gate, [0])
    qc.save_statevector(label=r'$\psi_i$')
    qc.ry(-theta, 0)
    qc.ry(gamma, 1)
    qc.cnot(0, 1)
    qc.measure(1, 0)
    qc.ry(theta, 0)
    qc.save_statevector(label=r'$\psi_f$')
    return qc

qc = get_weak_measurement_qc(alpha=0.5)
qc.draw('mpl')
```

### Testing weak measurement outcome probabilities

As an initial test of our weak measurement circuit, we simulate many measurements, calculate the estimated outcome probabilities from those data, and compare to the predicted probabilities.  To simplify, we choose $\ket{\psi}$ to lie in the X-Z plane of the Bloch sphere, so that we can describe the state with a single parameter $\theta$:

$$
\ket{\psi} = \cos{\frac{\theta}{2}} \ket{0} + \sin{\frac{\theta}{2}} \ket{1}
$$

In what follows, we'll choos $\theta = \pi / 4$, and measure in the $\mathrm{Z}$ basis, with $\alpha=0.5$.

```{code-cell} ipython3
from qiskit.visualization import plot_bloch_vector

theta = np.pi/4

plot_bloch_vector([1, theta, 0], coord_type='spherical', title=r'Initial $\psi$')
```

Now we simulate measuring with the same initial state many times and plot a histogram of the results:

```{code-cell} ipython3
from qiskit import transpile
from qiskit.providers.aer import AerSimulator

def simulate_weak_measurement(theta=0, alpha=0.5, shots=1):
    """Simulate performing `shots` trials of a weak measurement.

    Parameters
    ----------
    theta : float in range [0, pi], optional
        The polar angle of the initial state.
    alpha : float in range [0, 1], optional
        Controls the "weakness" of the measurement.  `alpha=0` corresponds
        to a strong measurment, `alpha=1` to a completely uninformative
        measurement.
    shots : int, optional
        The number of trials to perform.

    Returns
    -------
    qiskit.result.result.Result
        The output of the simulation(s).
    """
    psi = [np.cos(theta / 2), np.sin(theta /2)]

    qc = get_weak_measurement_qc(psi=psi, alpha=alpha)
    backend = AerSimulator()
    qc_compiled = transpile(qc, backend)
    job_sim = backend.run(qc_compiled, shots=shots)
    result_sim = job_sim.result()

    return result_sim
```

```{code-cell} ipython3
from qiskit.visualization import plot_histogram

alpha = 0.5
shots = 2048

results = simulate_weak_measurement(theta=theta, alpha=alpha, shots=shots)
counts = results.get_counts()

plot_histogram(counts, title=f'Weak measurement results for {shots} shots: \ntheta={theta:0.2f}, alpha={alpha}')
```

We estimate the measurement outcome probabilities by dividing the observed counts by the total number of shots:

```{code-cell} ipython3
p0_est = counts['0'] / shots
p1_est = counts['1'] / shots

print(f'Estimated p(m=0|theta): {p0_est:0.4f}')
print(f'Estimated p(m=1|theta): {p1_est:0.4f}')
```

We already computed the true outcome probabilities above - we need only plug in $\psi_0 = \cos{\frac{\theta}{2}}$ and $\psi_1 = \sin{\frac{\theta}{2}}$:

$$
p_0 = \frac{1}{1 + \alpha^2} \left( \cos^2{\frac{\theta}{2}} + \alpha^2 \sin^2{\frac{\theta}{2}} \right) \\
p_1 = \frac{1}{1 + \alpha^2} \left( \alpha^2 \cos^2{\frac{\theta}{2}} + \sin^2{\frac{\theta}{2}} \right)
$$

```{code-cell} ipython3
def get_p0(theta, alpha):
    return (np.cos(theta / 2)**2 + alpha**2 * np.sin(theta / 2)**2) / (1 + alpha**2)

def get_p1(theta, alpha):
    return (alpha**2 * np.cos(theta / 2)**2 + np.sin(theta / 2)**2) / (1 + alpha**2)

p0 = get_p0(theta, alpha)
p1 = get_p1(theta, alpha)

print(f'Actual p(m=0|theta): {p0:0.4f}')
print(f'Actual p(m=1|theta): {p1:0.4f}')
```

In the limit of infinite shots, these two pairs of probabilities should be exactly the same.  To be more quantitave, I'll use a binomial test, which tells me the probability of getting these results given that our theoretical value of $p_0$ is correct.  Typically, if $p$ is equal to or lower than $0.05$, we reject that hypothesis.

```{code-cell} ipython3
from scipy.stats import binomtest

b = binomtest(counts['0'], n=shots, p=p0)
print(f'p={b.pvalue:0.4f}')
```

### Testing resultant state after weak measurement

As a final check of our circuit, we ensure that the final state of our primary qubit is correct after measurement.

If we measure $m=0$, the system should be in the state $\ket{\psi_0'} \otimes \ket{0}$:

```{code-cell} ipython3
from qiskit.quantum_info import Statevector

normalization_factor = np.sqrt(np.cos(theta / 2)**2 + alpha**2 * np.sin(theta / 2)**2)
psi0_prime = np.array([np.cos(theta/2), alpha * np.sin(theta/2)]) / normalization_factor
system_psi0_prime_theoretical = Statevector([psi0_prime[0], psi0_prime[1], 0, 0])

print('If we measure 0, the system should be in the state:')
system_psi0_prime_theoretical.draw('latex')
```

```{code-cell} ipython3
# Get state after measuring 0
measured_0 = False
while not measured_0:
    result_0 = simulate_weak_measurement(theta=theta, alpha=alpha, shots=1)
    counts = result_0.get_counts()
    
    # Check if we measured 1 or 0
    measured_0 = (counts.get('0') == 1)

system_psi0_prime_experimental = result_0.data()['$\\psi_f$']

print('After measuring 0, the system was in the state:')
system_psi0_prime_experimental.draw('latex')
```

```{code-cell} ipython3
assert system_psi0_prime_experimental == system_psi0_prime_theoretical
```

Repeating this analysis for the case where we measure $m=1$, when the system should be in the state $\ket{\psi_1'} \otimes \ket{1}$:

```{code-cell} ipython3
normalization_factor = np.sqrt(alpha**2 * np.cos(theta / 2)**2 + np.sin(theta / 2)**2)
psi1_prime = np.array([alpha * np.cos(theta/2), np.sin(theta/2)]) / normalization_factor
system_psi1_prime_theoretical = Statevector([0, 0, psi1_prime[0], psi1_prime[1]])

print('If we measure 1, the system should be in the state:')
system_psi1_prime_theoretical.draw('latex')
```

```{code-cell} ipython3
# Get state after measuring 1
measured_1 = False
while not measured_1:
    result_1 = simulate_weak_measurement(theta=theta, alpha=alpha, shots=1)
    counts = result_1.get_counts()
    
    # Check if we measured 1 or 0
    measured_1 = (counts.get('1') == 1)

system_psi1_prime_experimental = result_1.data()['$\\psi_f$']

print('After measuring 1, the system was in the state:')
system_psi1_prime_experimental.draw('latex')
```

```{code-cell} ipython3
assert system_psi1_prime_experimental == system_psi1_prime_theoretical
```

### Comparison to strong measurement

To emphasize the difference between this procedure and a typical quantum measurement, consider the state of the primary qubit ("qubit 0") before and after the weak measurement that found $m=0$:

```{code-cell} ipython3
from qiskit.visualization import plot_bloch_multivector

psi_i = result_0.data()['$\\psi_i$']
psi_f = result_0.data()['$\\psi_f$']

plot_bloch_multivector(psi_i, title='Initial State')
```

```{code-cell} ipython3
plot_bloch_multivector(psi_f, title='Final State after measuring m=0')
```

Although the state was affected, the superposition has not been completely destroyed.  Compare to the action of a strong measurement:

```{code-cell} ipython3
# Get state after measuring 0
measured_0 = False
while not measured_0:
    result_strong_0 = simulate_weak_measurement(theta=theta, alpha=0, shots=1)
    counts = result_strong_0.get_counts()

    # Check if we measured 1 or 0
    measured_0 = (counts.get('0') == 1)

psi_f_strong = result_strong_0.data()['$\\psi_f$']

plot_bloch_multivector(psi_f_strong, title='Final state after strong measurement of m=0')
```

```{code-cell} ipython3

```
