# Weak Quantum Measurement

A typical "strong" quantum measurement collapses the wavefunction entirely after
measurement, meaning that successive measurements of the state aren't useful.  Weak
measurement, however, only slightly alters the original state - but pays for this by
revealing little information.

In this work, I implement weak measurement using (strong) projective
measurement on the Qiskit platform, and investigate the possibility of carefully using
successive weak measurements to reveal more about a given state than would be possible
with a single strong measurement.

```{tableofcontents}
```
